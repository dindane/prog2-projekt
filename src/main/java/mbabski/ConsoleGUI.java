package mbabski;

import java.util.Scanner;

public class ConsoleGUI extends GUI {
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Deklaracja semestralna przedmiotów na studiach magisterskich");
        WriteSubjects.writeToFile();

        DataBase instance = DataBase.getInstance();
        instance.loadCourses();
    }

    @Override
    public void showMainMenu() {

        System.out.println("Menu główne:" +
                "\n1 - Zasady wyboru przedmiotów" +
                "\n2 - Lista dostępnych przedmiotów" +
                "\n3 - Rozpoczęcie procedury wyboru przedmiotów" +
                "\n0 - Wyjście" +
                "\n");
        System.out.print("Wybierz nr polecenia (0-3): ");
        String s = sc.nextLine();
        while ((!s.matches("[0-9]+")) || ((Integer.parseInt(s) < 0) || (Integer.parseInt(s) > 3))) {
            System.out.print("Wybrano nieprawidłowy nr, spróbuj ponownie: ");
            s = sc.nextLine();
        }
        int choice = Integer.parseInt(s);
        switch (choice) {
            case 1:
                Subjects.printRules();
                showMainMenu();
                break;
            case 2:
                Subjects.printSubjects();
                showMainMenu();
                break;
            case 3:
                System.out.print("Wpisz nr semestru, na który chcesz wybrać przedmioty (1-4): ");
                String s2 = sc.nextLine();
                while ((!s2.matches("[0-9]+")) || ((Integer.parseInt(s2) < 0) || (Integer.parseInt(s2) > 4))) {
                    System.out.print("Wybrano nieprawidłowy nr, spróbuj ponownie: ");
                    s2 = sc.nextLine();
                }
                int number = Integer.parseInt(s2);
                Semester semester;
                switch (number) {
                    case 1:
                        semester = new Semester(1);
                        semester.start();
                        break;
                    case 2:
                        semester = new Semester(2);
                        semester.start();
                        break;
                    case 3:
                        semester = new Semester(3);
                        semester.start();
                        break;
                    case 4:
                        semester = new Semester(4);
                        semester.start();
                        break;
                    default:
                        showMainMenu();
                }
                break;

            default:
                System.out.print("");
        }

    }
}