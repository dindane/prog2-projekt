package mbabski;

import java.io.Serializable;

public class Courses implements Serializable {
    private String signature;
    private String subject;
    private double ects;

    public Courses(){}

    public Courses(String signature, String subject) {
        this.signature = signature;
        this.subject = subject;
    }

    public Courses(String signature, String subject, double ects) {
        this.signature = signature;
        this.subject = subject;
        this.ects = ects;
    }

    public String getSignature() {
        return signature;
    }

    public String getSubject() {
        return subject;
    }

    public double getEcts() {
        return ects;
    }

}
