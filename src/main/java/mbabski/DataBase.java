package mbabski;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class DataBase {
    private List<Subjects> subjects;
    private List<Courses> courses;

    private static final DataBase INSTANCE = new DataBase();

    public List<Subjects> getSubjects() {
        return subjects;
    }

    public List<Courses> getCourses() {
        return courses;
    }

    private DataBase() {
    }

    public static DataBase getInstance(){
        return INSTANCE;
    }
    public void loadSubjects() {
        try (ObjectInputStream in = new ObjectInputStream(Files.newInputStream(Paths.get("subjects.txt")))) {
            subjects =(ArrayList<Subjects>) in.readObject();
        } catch (
                IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void loadCourses() {
        try (ObjectInputStream in = new ObjectInputStream(Files.newInputStream(Paths.get("courses.txt")))) {
            courses = (ArrayList<Courses>) in.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
