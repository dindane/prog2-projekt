package mbabski;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Semester {
    private List<Subjects> chosenCourses;
    private int semesterNo;

    public List<Subjects> getChosenCourses() {
        return chosenCourses;
    }

    public int getSemesterNo() {
        return semesterNo;
    }

    public Semester(int semesterNo) {
        chosenCourses = new ArrayList<Subjects>();
        this.semesterNo = semesterNo;
    }

    public void start() {
        switch (this.semesterNo) {
            case 1:
                this.chosenCourses.add(new Subjects("201010-0001", "Lektorat z języka obcego",
                        "Nowak Halina", DayOfWeek.MONDAY, LocalTime.of(17, 10), "C-201", 4.5));
                addLecturer("21010");
                addLecturer("21011");
                addLecturer("22209");
                addLecturer("22212");
                addLecturer("22213");
                addFieldCourse();
                break;
            case 2:
                this.chosenCourses.add(new Subjects("201020-0001", "Lektorat z języka obcego",
                        "Tomczyk Zofia", DayOfWeek.THURSDAY, LocalTime.of(17, 10), "C-201", 4.5));
                addFieldCourse();
                addFieldCourse();
                addFieldCourse();
                addFieldCourse();
                addFieldCourse();
                addFieldCourse();
                break;
            case 3:
                this.chosenCourses.add(new Subjects("290000-0001", "Seminarium magisterskie", "",
                        DayOfWeek.MONDAY, LocalTime.of(19, 00), "", 8.0));
                addFieldCourse();
                addFieldCourse();
                addFieldCourse();
                addFieldCourse();
                addFieldCourse();
                addFieldCourse();
                break;
            default:
                this.chosenCourses.add(new Subjects("290020-0001", "Seminarium magisterskie", "",
                        DayOfWeek.TUESDAY, LocalTime.of(19, 00), "", 12.0));
                addCourse();
                addCourse();
                addCourse();
                addCourse();
                addCourse();
                addCourse();
        }
        chooseAction();
    }

    public void chooseAction() {
        System.out.println("\nWybierz akcję z poniższej listy:" +
                "\n1 - Wyświetl listę wszystkich przedmiotów" +
                "\n2 - Dodaj przedmiot do koszyka" +
                "\n3 - Usuń przedmiot z koszyka" +
                "\n4 - Wyświetl listę przedmiotów dodanych do koszyka" +
                "\n5 - Wyświetl tygodniowy harmonogram zajęć" +
                "\n6 - Koszyk -> Deklaracja (wysyłka do dziekanatu)" +
                "\n\nWybierz nr polecenia (1-6): ");

        String s = ConsoleGUI.sc.nextLine();
        while ((!s.matches("[0-9]+")) || ((Integer.parseInt(s) < 0) || (Integer.parseInt(s) > 6))) {
            System.out.print("Wybrano nieprawidłowy nr, spróbuj ponownie: ");
            s = ConsoleGUI.sc.nextLine();
        }
        int choice = Integer.parseInt(s);
        switch (choice) {
            case 1:
                Subjects.printSubjects();
                chooseAction();
                break;
            case 2:
                addCourse();
                chooseAction();
                break;
            case 3:
                removeCourse();
                chooseAction();
                break;
            case 4:
                printList();
                chooseAction();
                break;
            case 5:
                printSchedule();
                chooseAction();
                break;
            case 6:
                declareBasket();
                break;
            default:
                System.out.println("Wybrano nieprawidłowy nr, spróbuj ponownie:");
                chooseAction();
                break;
        }
    }

    public static String findCourse(String signature) {
        for (int i = 0; i < DataBase.getInstance().getCourses().size(); i++) {
            if (signature.equals(DataBase.getInstance().getCourses().get(i).getSignature())) {
                return DataBase.getInstance().getCourses().get(i).getSubject();
            }
        }
        return "";
    }

    public void addCourse() {
        Subjects.printSubjects();
        boolean flag = true;
        String s = "";
        while (flag) {
            int count = 0;
            System.out.print("Wpisz sygnaturę przedmiotu: ");
            s = ConsoleGUI.sc.nextLine();
            for (int i = 1; i < this.chosenCourses.size(); i++) {
                if (s.equals(this.chosenCourses.get(i).getSignature().substring(0, 5))) {
                    count++;
                }
            }
            if (count != 0) {
                System.out.println("Przedmiot o wybranej sygnaturze został już wcześniej dodany do" +
                        " koszyka. Wybierz inny przedmiot.");
            } else {
                flag = false;
            }
        }
        flag = true;
        while (flag) {
            int count = 0;
            for (int i = 0; i < DataBase.getInstance().getCourses().size(); i++) {
                if (s.equals(DataBase.getInstance().getCourses().get(i).getSignature())) {
                    count++;
                }
            }
            if (count == 0) {
                System.out.print("Wpisano niepoprawną sygnaturę przedmiotu, spróbuj jeszcze raz: ");
                s = ConsoleGUI.sc.nextLine();
            } else {
                flag = false;
            }
        }
        addLecturer(s);
    }

    public void addFieldCourse() {
        System.out.println("Wybierz przedmiot z listy przedmiotów kierunkowych:");
        Subjects.printFieldSubjects();
        boolean flag = true;
       String s="";
        while (flag) {
            int count = 0;
            System.out.print("\nWpisz sygnaturę przedmiotu: ");
            s = ConsoleGUI.sc.nextLine();
            for (int i = 1; i < this.chosenCourses.size(); i++) {
                if (s.equals(this.chosenCourses.get(i).getSignature().substring(0, 5))) {
                    count++;
                }
            }
            if (count != 0) {
                System.out.println("Przedmiot o wybranej sygnaturze został już wcześniej dodany do koszyka." +
                        " Wybierz inny przedmiot.");
            } else {
                flag = false;
            }
        }
        flag = true;
        while (flag) {
            int count = 0;
            for (int i = 2; i < 28; i++) {
                if (s.equals(DataBase.getInstance().getCourses().get(i).getSignature())) {
                    count++;
                }
            }
            if (count == 0) {
                System.out.print("Wpisano niepoprawną sygnaturę przedmiotu, spróbuj jeszcze raz: ");
                s = ConsoleGUI.sc.nextLine();
            } else {
                flag = false;
            }
        }
        addLecturer(s);
    }

    public void addLecturer(String signature) {
        System.out.println("\nAby wybrać wykładowcę dla przedmiotu \"" + findCourse(signature) +
                "\" wpisz ostatnie 4 cyfry jego sygnatury:\n");
        int i;
        int end = 0;
        int width = 0;
        for (i = 0; i < DataBase.getInstance().getSubjects().size(); i++) {
            if (signature.equals(DataBase.getInstance().getSubjects().get(i).getSignature().
                    substring(0, 5))) {
                end = i;
                width++;
            }
        }
        for (i = (end - width + 1); i <= end; i++) {
            System.out.println(DataBase.getInstance().getSubjects().get(i).getSignature() + ": " +
                    DataBase.getInstance().getSubjects().get(i).getSubject() + " | " +
                    DataBase.getInstance().getSubjects().get(i).getLecturer() + " | " +
                    DataBase.getInstance().getSubjects().get(i).getDay() + " " +
                    DataBase.getInstance().getSubjects().get(i).getHour() + "-" +
                    DataBase.getInstance().getSubjects().get(i).getHour().plusMinutes(100) + " | " +
                    DataBase.getInstance().getSubjects().get(i).getVenue());
        }
        System.out.print("\nWybór: ");
        String choice;
        int count = 0;
        int chosen = 0;
        while (count == 0) {
            choice = ConsoleGUI.sc.nextLine();
            for (i = (end - width + 1); i <= end; i++) {
                if (choice.equals(DataBase.getInstance().getSubjects().get(i).getSignature().
                        substring(7, 11))) {
                    count++;
                    chosen = i;
                }
            }
            if (count == 0) {
                System.out.print("Podano nieprawidłową wartość, spróbuj jeszcze raz: ");
            }
        }
        for (int j = 0; j < this.chosenCourses.size(); j++) {
            if (DataBase.getInstance().getSubjects().get(chosen).getDay().equals(this.chosenCourses.
                    get(j).getDay()) &&
                    DataBase.getInstance().getSubjects().get(chosen).getHour().equals
                            (this.chosenCourses.get(j).getHour())) {
                System.out.println("Wybrany przedmiot koliduje terminem zajęć z przedmiotem " +
                        this.chosenCourses.get(j).getSubject() + " o sygnaturze "
                        + this.chosenCourses.get(j).getSignature() + ".\nUsuń kolidujący przedmiot" +
                        " z koszyka bądź, jeśli to możliwe, wybierz innego wykładowcę.");
                chooseAction();
            }
        }
        this.chosenCourses.add(DataBase.getInstance().getSubjects().get(chosen));
        System.out.println("Pomyślnie dodano:\n" + DataBase.getInstance().getSubjects().get(chosen).
                getSignature() + ": " +
                DataBase.getInstance().getSubjects().get(chosen).getSubject() + " | " +
                DataBase.getInstance().getSubjects().get(chosen).getLecturer() + " | " +
                DataBase.getInstance().getSubjects().get(chosen).getDay() + " " +
                DataBase.getInstance().getSubjects().get(chosen).getHour() + "-" +
                DataBase.getInstance().getSubjects().get(chosen).getHour().plusMinutes(100) + " | " +
                DataBase.getInstance().getSubjects().get(chosen).getVenue() + "\n");
    }

    public double ectsInBasket(List<Subjects> list) {
        return list.stream().mapToDouble(x -> x.getEcts()).sum();
    }

    public void printList() {
        System.out.println("Lista aktualnie zadeklarowanych przedmiotów:");
        for (int i = 0; i < this.chosenCourses.size(); i++) {
            System.out.println((i + 1) + ". " + this.chosenCourses.get(i).getSignature() +
                    ": " + this.chosenCourses.get(i).getSubject() +
                    " | " + this.chosenCourses.get(i).getLecturer() + " | " +
                    this.chosenCourses.get(i).getDay() + " " + this.chosenCourses.get(i).getHour() +
                    "-" + this.chosenCourses.get(i).getHour().plusMinutes(100) + " | " +
                    this.chosenCourses.get(i).getVenue() + " | ECTS: " +
                    this.chosenCourses.get(i).getEcts());
        }
        double points = ectsInBasket(this.chosenCourses);
        if (points < 30) {
            System.out.println("\nŁącznie: " + points + " pkt ECTS. Zadeklaruj jeszcze przedmioty za " +
                    "przynajmniej " + (30 - points) + " pkt ECTS.\n");
        } else {
            System.out.println("\nŁącznie: " + points + " pkt ECTS.\n");
        }
    }

    public void removeCourse() {
        printList();
        System.out.print("Wybierz nr, przedmiotu, który chcesz usunąć lub '0', aby wrócić do" +
                " poprzedniego menu: ");

        String s = ConsoleGUI.sc.nextLine();
        while ((!s.matches("[0-9]+")) || ((Integer.parseInt(s) < 2) && (Integer.parseInt(s) != 0))
                || (Integer.parseInt(s) > this.chosenCourses.size())) {
            System.out.print("Wybrano nieprawidłowy nr przedmiotu, spróbuj ponownie: ");
            s = ConsoleGUI.sc.nextLine();
        }
        int choice = Integer.parseInt(s);
        if (choice == 0) {
            chooseAction();
        } else {
            System.out.println("Usunięto z listy przedmiot:\n" +
                    (choice) + ". " + this.chosenCourses.get(choice - 1).getSignature() + ": " +
                    this.chosenCourses.get(choice - 1).getSubject() +
                    " | " + this.chosenCourses.get(choice - 1).getLecturer() + " | " +
                    this.chosenCourses.get(choice - 1).getDay() + " " +
                    this.chosenCourses.get(choice - 1).getHour() + "-" +
                    this.chosenCourses.get(choice - 1).getHour().plusMinutes(100) + " | " +
                    this.chosenCourses.get(choice - 1).getVenue() + " | ECTS: " +
                    this.chosenCourses.get(choice - 1).getEcts() + "\n");
            this.chosenCourses.remove(choice - 1);
            chooseAction();
        }
    }

    public void printSchedule() {
        List<Subjects> sortedList = chosenCourses.stream()
                .sorted(Comparator.comparing(Subjects::getDay).thenComparing(Subjects::getHour))
                .collect(Collectors.toList());

        for (int i = 0; i < 7; i++) {
            System.out.println("\n" + DayOfWeek.values()[i]);
            int count = 0;
            for (int j = 0; j < sortedList.size(); j++) {
                if (sortedList.get(j).getDay().ordinal() == i) {
                    count++;
                    System.out.println(sortedList.get(j).getHour() + "-" +
                            sortedList.get(j).getHour().plusMinutes(100) + " | " + sortedList.get(j).
                            getSubject() + " (" + sortedList.get(j).getLecturer() + ") | " +
                            sortedList.get(j).getVenue());
                }
            }
            if (count == 0) {
                System.out.println("Wolne!");
            }
        }
        System.out.println();
        chooseAction();
    }

    public void declareBasket() {
        int check1 = 0;
        int check2 = 0;
        int check3 = 0;
        int check4 = 0;
        int check5 = 0;
        int check6 = 0;

        for (int i = 1; i < this.chosenCourses.size(); i++) {
            if (this.chosenCourses.get(i).getSignature().substring(0, 5).equals("21010")) {
                check1++;
                break;
            }
        }
        for (int i = 1; i < this.chosenCourses.size(); i++) {
            if (this.chosenCourses.get(i).getSignature().substring(0, 5).equals("21011")) {
                check2++;
                break;
            }
        }
        for (int i = 1; i < this.chosenCourses.size(); i++) {
            if (this.chosenCourses.get(i).getSignature().substring(0, 5).equals("22209")) {
                check3++;
                break;
            }
        }
        for (int i = 1; i < this.chosenCourses.size(); i++) {
            if (this.chosenCourses.get(i).getSignature().substring(0, 5).equals("22212")) {
                check4++;
                break;
            }
        }
        for (int i = 1; i < this.chosenCourses.size(); i++) {
            if (this.chosenCourses.get(i).getSignature().substring(0, 5).equals("22213")) {
                check5++;
                break;
            }
        }
        switch (this.semesterNo) {
            case 1:
                for (int i = 1; i < this.chosenCourses.size(); i++) {
                    for (int j = 5; j < 28; j++) {
                        if (this.chosenCourses.get(i).getSignature().substring(0, 5).equals(DataBase.
                                getInstance().getCourses().get(j).getSignature())) {
                            check6++;
                            break;
                        }
                    }
                }
                if (check1 * check2 * check3 * check4 * check5 * check6 == 1) {
                    System.out.print("");
                } else {
                    if (check1 == 0) {
                        System.out.println("Błąd! Nie wybrano przedmiotu \"Historia myśli ekonomicznej\".");
                    }
                    if (check2 == 0) {
                        System.out.println("Błąd! Nie wybrano przedmiotu \"Prawo gospodarcze\".");
                    }
                    if (check3 == 0) {
                        System.out.println("Błąd! Nie wybrano przedmiotu \"Ekonomia menedżerska\".");
                    }
                    if (check4 == 0) {
                        System.out.println("Błąd! Nie wybrano przedmiotu \"Ekonomia rozwoju\".");
                    }
                    if (check5 == 0) {
                        System.out.println("Błąd! Nie wybrano przedmiotu \"Ekonomia sektora publicznego\".");
                    }
                    if (check6 == 0) {
                        System.out.println("Błąd! Nie wybrano przedmiotu przedmiotu kierunkowego.");
                    }
                    chooseAction();
                }
                break;
            case 2:
            case 3:
                if (check1 != 0) {
                    System.out.println("Błąd! Wybrano przedmiot \"Historia myśli ekonomicznej\", " +
                            "który został zrealizowany w I semestrze. Usuń przedmiot z koszyka.");
                }
                if (check2 != 0) {
                    System.out.println("Błąd! Wybrano przedmiot \"Prawo gospodarcze\", " +
                            "który został zrealizowany w I semestrze. Usuń przedmiot z koszyka.");
                }
                if (check3 != 0) {
                    System.out.println("Błąd! Wybrano przedmiot \"Ekonomia menedżerska\", " +
                            "który został zrealizowany w I semestrze. Usuń przedmiot z koszyka.");
                }
                if (check4 != 0) {
                    System.out.println("Błąd! Wybrano przedmiot \"Ekonomia rozwoju\", " +
                            "który został zrealizowany w I semestrze. Usuń przedmiot z koszyka.");
                }
                if (check5 != 0) {
                    System.out.println("Błąd! Wybrano przedmiot \"Ekonomia sektora publicznego\", " +
                            "który został zrealizowany w I semestrze. Usuń przedmiot z koszyka.");
                }
                if (check1 + check2 + check3 + check4 + check5 > 0) {
                    chooseAction();
                }

                int check7 = 0;
                for (int i = 1; i < this.chosenCourses.size(); i++) {
                    for (int j = 5; j < 17; j++) {
                        if (this.chosenCourses.get(i).getSignature().substring(0, 5).equals(DataBase.
                                getInstance().getCourses().get(j).getSignature())) {
                            check7++;
                        }
                    }
                }
                if (check7 < 6) {
                    System.out.println("Nie wybrano wymaganej liczby przedmiotów kierunkowych." +
                            " Dobierz jeszcze przedmioty kierunkowe w liczbie przynajmniej: " + (6 - check7)
                            + ".");
                    chooseAction();
                }
                break;
            default:
                if (check1 != 0) {
                    System.out.println("Błąd! Wybrano przedmiot \"Historia myśli ekonomicznej\", " +
                            "który został zrealizowany w I semestrze. Usuń przedmiot z koszyka.");
                }
                if (check2 != 0) {
                    System.out.println("Błąd! Wybrano przedmiot \"Prawo gospodarcze\", " +
                            "który został zrealizowany w I semestrze. Usuń przedmiot z koszyka.");
                }
                if (check3 != 0) {
                    System.out.println("Błąd! Wybrano przedmiot \"Ekonomia menedżerska\", " +
                            "który został zrealizowany w I semestrze. Usuń przedmiot z koszyka.");
                }
                if (check4 != 0) {
                    System.out.println("Błąd! Wybrano przedmiot \"Ekonomia rozwoju\", " +
                            "który został zrealizowany w I semestrze. Usuń przedmiot z koszyka.");
                }
                if (check5 != 0) {
                    System.out.println("Błąd! Wybrano przedmiot \"Ekonomia sektora publicznego\", " +
                            "który został zrealizowany w I semestrze. Usuń przedmiot z koszyka.");
                }
                if (check1 + check2 + check3 + check4 + check5 > 0) {
                    chooseAction();
                }
        }
        double ects = ectsInBasket(this.chosenCourses);
        if (ects < 30.0) {
            System.out.println("Wybrano za mało przedmiotów. Dodaj do koszyka przedmioty o wartości " +
                    "przynajmniej " + (30.0 - ects) + " pkt ECTS.");
            chooseAction();
        }
        if (ects > 70.0) {
            System.out.println("Wybrano za dużo przedmiotów. Usuń z koszyka przedmioty o wartości " +
                    "przynajmniej " + (ects - 70.0) + " pkt ECTS.");
            chooseAction();
        }
        System.out.println("Przedmioty zawarte w koszyku poprawnie dodano do deklaracji i wysłano " +
                    "do dziekanatu.");
        System.exit(0);
    }
}