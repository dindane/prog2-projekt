package mbabski;

public abstract class GUI {
    public GUI(){
        DataBase.getInstance().loadSubjects();
        DataBase.getInstance().loadCourses();
    }
    public abstract void showMainMenu();
}
