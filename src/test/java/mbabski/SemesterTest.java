package mbabski;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class SemesterTest {

    Semester sem;

    @Test
    void start() {
        //when
        int semNo = sem.getSemesterNo();

    }

    @Test
    void chooseAction() {
    }

    @Test
    void findCourse() {
        //given
        //  sem = mock(Semester.class);
        sem = new Semester(1);
        DataBase.getInstance();
        //when
        String signature = "22212";
        //then
        assertEquals("Ekonomia rozwoju", Semester.findCourse(signature));
    }

    @Test
    void addCourse() {
    }

    @Test
    void addFieldCourse() {
    }

    @Test
    void addLecturer() {
    }

    @Test
    void ectsInBasket() {
    }

    @Test
    void printList() {
    }

    @Test
    void removeCourse() {
    }

    @Test
    void printSchedule() {
    }

    @Test
    void declareBasket() {
    }
}